package com.matrix.jaxrs;

import javax.ws.rs.core.Response;

public class ApiResult {

	private int status;
	private String message;
	Object result;

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}

	public static Response buildResponse(ApiResult res) {
		return Response
        		.ok()
                .header("Access-Control-Allow-Origin", "*")
                .entity(res)
                .build();
	}
}
