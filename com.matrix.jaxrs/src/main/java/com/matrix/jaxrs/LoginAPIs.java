package com.matrix.jaxrs;

import java.util.Base64;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/token")
public class LoginAPIs {

	public LoginAPIs() {
	}
	
    @POST
    @Path("/generate-token")
    @Consumes("application/json")
    @Produces("application/json")    
    public Response generateToken(UserLogin u) {
    	System.out.println("Serving POST on /generate-token, username="+u.getUsername());
    	ApiResult res = new ApiResult();
    	res.setStatus(200);
    	res.setMessage("Successful login");
    	UserToken u1 = new UserToken();
    	u1.setUsername(u.getUsername());
    	String tk = u.getUsername()+"/"+u.getPassword();
		u1.setToken(Base64.getEncoder().encodeToString(tk.getBytes()));
    	res.setResult(u1);
    	return ApiResult.buildResponse(res);
    }

    @GET
    @Path("/generate-token")
    @Produces("application/json")    
    public Response generateToken() {
    	UserLogin u = new UserLogin();
    	u.setUsername("test username");
    	u.setPassword("test password");
    	return generateToken(u);
    }

}
