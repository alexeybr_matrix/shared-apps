package com.matrix.jaxrs;

import java.sql.SQLException;
import java.util.Collection;

public interface ICustomersRepository {
    CustomerData getCustomer(String id) throws Exception;

    CustomerData addCustomer(CustomerData u) throws Exception;

    CustomerData addCustomer(CustomerData u, int id) throws Exception;

    Collection<CustomerData> getCustomers() throws Exception;

    void deleteCustomer(int id) throws Exception;

    void deleteCustomer(String id) throws Exception;

    void updateCustomer(CustomerData u, int id) throws Exception;
}
