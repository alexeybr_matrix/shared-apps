package com.matrix.jaxrs.impl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.matrix.jaxrs.CustomerData;
import com.matrix.jaxrs.ICustomersRepository;

public class CustomersRepository implements ICustomersRepository {
    // As this is static, the VM makes sure that this is called only once
    private static final CustomersRepository INSTANCE = new CustomersRepository();
    private DataSource _ds = null;
    private Connection _conn;

    // We know the contstructor is called only once. Making it private
    // guarentees no other classes can call it.
    // ==> Thus this is a nice safe place to initialize your Hash
    private CustomersRepository() {
    }

    public static CustomersRepository getInstance() {
        return INSTANCE;
    }

    private DataSource getDataSource() throws NamingException {
        if (_ds == null) {
            final Context ctx = new InitialContext();
            _ds = (DataSource) ctx.lookup("jndi/ExampleDS");
        }
        return _ds;
    }

    private Connection getConnection() throws NamingException, SQLException {
        if(_conn == null || _conn.isClosed()) {
            _conn = getDataSource().getConnection();
        }
        return _conn;
    }

    private void rescindConnection() throws SQLException {
        if(_conn != null) {
            _conn.close();
            _conn = null;
        }
    }

    @Override
    public CustomerData getCustomer(String id) throws SQLException, NamingException {
        PreparedStatement pstmt = null;
        try {
            pstmt = getConnection().prepareStatement("select * from customers where customerNumber = ?");
            pstmt.setString(1, id);
            ResultSet rs = pstmt.executeQuery();
            if(!rs.next())
                return null;

            CustomerData cd = new CustomerData();
            cd.setCustomerNumber(rs.getInt("customerNumber"));
            cd.setCustomerName(rs.getString("customerName"));
            cd.setContactLastName(rs.getString("contactLastName"));
            cd.setContactFirstName(rs.getString("contactFirstName"));
            cd.setPhone(rs.getString("phone"));
            cd.setAddressLine1(rs.getString("addressLine1"));
            cd.setCity(rs.getString("city"));
            cd.setState(rs.getString("state"));
            // cd.set(rs.getString("postalCode"));
            return cd;
        } finally {
            if(pstmt != null) pstmt.close();
            rescindConnection();
        }
    }

    @Override
    public CustomerData addCustomer(CustomerData u) throws SQLException, NamingException {
        CustomerData oldData = getCustomer(Integer.toString(u.getId()));
        PreparedStatement pstmt = null;
        try {
            getConnection().setAutoCommit(false);
            pstmt = getConnection().prepareStatement(
                oldData != null ?
                "UPDATE customers SET customerName = ?, contactLastName = ?, contactFirstName = ?, "
                + " phone = ?, addressLine1 = ?, city = ?, state = ? "
                + " WHERE customerNumber = ?"
                :
                "INSERT INTO customers (customerName, contactLastName, contactFirstName, " 
                + " phone, addressLine1, city, state, customerNumber) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, u.getCustomerName());
            pstmt.setString(2, u.getContactLastName());
            pstmt.setString(3, u.getContactFirstName());
            pstmt.setString(4, u.getPhone());
            pstmt.setString(5, u.getAddressLine1());
            pstmt.setString(6, u.getCity());
            pstmt.setString(7, u.getState());
            pstmt.setInt(8, u.getCustomerNumber());
            pstmt.executeUpdate();
            getConnection().commit();
        } finally {
            if (pstmt != null)
                pstmt.close();
            rescindConnection();
        }
        return oldData;
    }

    @Override
    public CustomerData addCustomer(CustomerData u, int id) throws SQLException, NamingException {
        u.setId(id);
        return addCustomer(u);
    }

    @Override
    public Collection<CustomerData> getCustomers() throws SQLException, NamingException {
        PreparedStatement pstmt = null;
        try {
            pstmt = getConnection().prepareStatement("select * from customers");
            ResultSet rs = pstmt.executeQuery();
            ArrayList<CustomerData> result = new ArrayList<CustomerData>();
            while(rs.next())
            {
                CustomerData cd = new CustomerData();
                cd.setCustomerNumber(rs.getInt("customerNumber"));
                cd.setCustomerName(rs.getString("customerName"));
                cd.setContactLastName(rs.getString("contactLastName"));
                cd.setContactFirstName(rs.getString("contactFirstName"));
                cd.setPhone(rs.getString("phone"));
                cd.setAddressLine1(rs.getString("addressLine1"));
                cd.setCity(rs.getString("city"));
                cd.setState(rs.getString("state"));
                // cd.set(rs.getString("postalCode"));
                result.add(cd);
            }
            return result;
        } finally {
            if (pstmt != null)
                pstmt.close();
            rescindConnection();
        }
    }

    @Override
    public void deleteCustomer(int id) throws SQLException, NamingException {
        PreparedStatement pstmt = null;
        try {
            getConnection().setAutoCommit(false);
            pstmt = getConnection().prepareStatement("DELETE FROM customers WHERE customerNumber = ?");
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            getConnection().commit();
        } finally {
            if (pstmt != null)
                pstmt.close();
            rescindConnection();
        }
    }

    @Override
    public void deleteCustomer(String id) throws SQLException, NamingException {
        PreparedStatement pstmt = null;
        try {
            getConnection().setAutoCommit(false);
            pstmt = getConnection().prepareStatement(
                    "DELETE FROM customers WHERE customerNumber = ?");
            pstmt.setString(1, id);
            pstmt.executeUpdate();
            getConnection().commit();
        } finally {
            if (pstmt != null)
                pstmt.close();
            rescindConnection();
        }
    }

    @Override
    public void updateCustomer(CustomerData u, int id) throws SQLException, NamingException {
        PreparedStatement pstmt = null;
        try {
            getConnection().setAutoCommit(false);
            pstmt = getConnection().prepareStatement(
                 "UPDATE customers SET customerName = ?, contactLastName = ?, contactFirstName = ?, "
                            + " phone = ?, addressLine1 = ?, city = ?, state = ? " + " WHERE customerNumber = ?"
                );
            pstmt.setString(1, u.getCustomerName());
            pstmt.setString(2, u.getContactLastName());
            pstmt.setString(3, u.getContactFirstName());
            pstmt.setString(4, u.getPhone());
            pstmt.setString(5, u.getAddressLine1());
            pstmt.setString(6, u.getCity());
            pstmt.setString(7, u.getState());
            pstmt.setInt(8, u.getCustomerNumber());
            pstmt.executeUpdate();
            getConnection().commit();
        } finally {
            if (pstmt != null)
                pstmt.close();
            rescindConnection();
        }
    }
    
}
