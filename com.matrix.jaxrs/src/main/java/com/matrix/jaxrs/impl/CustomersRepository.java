package com.matrix.jaxrs.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;

import com.matrix.jaxrs.CustomerData;
import com.matrix.jaxrs.ICustomersRepository;

public class CustomersRepository implements ICustomersRepository {

    private final Hashtable<String, CustomerData> customers;

    // As this is static, the VM makes sure that this is called only once
    private static final CustomersRepository INSTANCE = new CustomersRepository(); 

    // We know the contstructor is called only once.  Making it private
    // guarentees no other classes can call it.
    // ==> Thus this is a nice safe place to initialize your Hash
    private CustomersRepository() {
        customers = new Hashtable<String, CustomerData>();
    }

    public static CustomersRepository getInstance() {
        return INSTANCE;
    }

   public final CustomerData getCustomer(String id) {
       return customers.get(id); 
   }	

   public final CustomerData addCustomer(CustomerData u) {
	   u.setId(customers.keySet().isEmpty() ? 1 : 
			   Integer.valueOf(Collections.max(customers.keySet()))+1);
       return customers.put(String.valueOf(u.getId()),u); 
   }

   public final CustomerData addCustomer(CustomerData u, int id) {
	   u.setId(id);
       return customers.put(String.valueOf(u.getId()),u); 
   }	

   public final Collection<CustomerData> getCustomers() {
       return customers.values();
       //.toArray(new UserDetails[0]); 
   }	

   public final void deleteCustomer(int id){
	   customers.remove(String.valueOf(id));
   }

	public final void deleteCustomer(String id) {
		customers.remove(id);
	}

	public final void updateCustomer(CustomerData u, int id) {
		deleteCustomer(id);
		addCustomer(u, id);
	}
}
