package com.matrix.jaxrs;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.matrix.jaxrs.impl.jdbc.CustomersRepository;

//import org.json.JSONObject;

@Path("/customers")
public class CustomersAPIs {

    static {
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	private static void init() throws Exception {
		System.out.println(" *********************** API INIT *************************** ");
		CustomerData u = new CustomerData();
		u.setId(11);
		u.setCustomerName("Test customer");
		u.setContactFirstName("Person");
		u.setContactLastName("Family");
		u.setPhone("00-123456789");
		u.setAddressLine1("The address of this customer");
		u.setCity("New Town");
		u.setState("ST");
		CustomersRepository().addCustomer(u);
	}

    private static ICustomersRepository CustomersRepository() {
        return CustomersRepository.getInstance();
    }

    @GET
    @Produces("application/json")
    public Response getUsers() throws Exception {
    	System.out.println("Serving GET on /customers");
        Collection<CustomerData> c = CustomersRepository().getCustomers();
        System.out.println("Returning "+c.size()+" entries");

        ApiResult res = new ApiResult();
    	res.setStatus(200);
    	res.setMessage("Customers list fetched");
        res.setResult(c);
        return ApiResult.buildResponse(res);
    }


    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response getUser(@PathParam("id") String id) throws Exception {
    	System.out.println("Serving GET on /customers/{id}, id="+id);
        CustomerData c = CustomersRepository().getCustomer(id);
        //System.out.println("Resulting entity: "+(new JSONObject(c)));

        ApiResult res = new ApiResult();        
    	res.setStatus(200);
    	res.setMessage("Customer fetched");
        res.setResult(c);
        return ApiResult.buildResponse(res);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response addUser(CustomerData u) throws Exception {
    	System.out.println("Serving POST on /customers");
    	//System.out.println("Received object: "+(new JSONObject(u)));
        CustomersRepository().addCustomer(u);

        ApiResult res = new ApiResult();        
    	res.setStatus(200);
    	res.setMessage("Customer created");
        res.setResult(u);
        return ApiResult.buildResponse(res);        
    }

    @PUT
    @Path("{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateUser(@PathParam("id") String id, CustomerData u) throws NumberFormatException, Exception {
    	System.out.println("Serving PUT on /customers/{id}, id="+id);
        CustomersRepository().updateCustomer(u, Integer.valueOf(id));

        ApiResult res = new ApiResult();
    	res.setStatus(200);
    	res.setMessage("Customer updated");
        res.setResult(u);
        return ApiResult.buildResponse(res);        
    }

    /*
    @DELETE
    @Consumes("application/json")
    @Produces("application/json")
    public void deleteUser(CustomerData u) {
    	System.out.println("Serving DELETE on /customers");
    	System.out.println("Received object: "+u);
        CustomersRepository.getInstance().deleteUser(u);
    }
    */
    
    @DELETE
    @Path("{id}")
    @Produces("application/json")
    public Response deleteUser(@PathParam("id") String id) throws Exception {
    	System.out.println("Serving DELETE on /customers/{id}, id="+id);
        CustomersRepository().deleteCustomer(id);
        
        ApiResult res = new ApiResult();
    	res.setStatus(200);
    	res.setMessage("Customer deleted");
        res.setResult(null);
        return ApiResult.buildResponse(res);
    }

}