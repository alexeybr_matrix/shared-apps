import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {User} from "../model/user.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }
  //urlOpts:string[] = new Array('http://34.77.93.62:9080/jaxrs/','http://35.189.196.111:9080/jaxrs/');
  urlBase:string = '/jaxrs/api/';
  //urlBase:string = 'http://localhost:4200/api/';

  getBaseUrl(apiPath: string) : string {
	//return this.urlOpts[Math.floor(Math.random() * this.urlOpts.length)] + apiPath;  
	return this.urlBase + apiPath;
  }
  
  login(loginPayload) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.getBaseUrl('token/generate-token'), loginPayload);
  }

  getUsers() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.getBaseUrl('customers/'));
  }

  getUserById(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.getBaseUrl('customers/') + id);
  }

  createUser(user: User): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.getBaseUrl('customers/'), user);
  }

  updateUser(user: User): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.getBaseUrl('customers/') + user.id, user);
  }

  deleteUser(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.getBaseUrl('customers/') + id);
  }
}
